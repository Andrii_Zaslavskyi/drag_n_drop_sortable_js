const dragAndDropItems = document.getElementById('team_members');

new Sortable(dragAndDropItems, {
    animation: 350,
    chosenClass: 'team_member_chosen',
    dragClass: 'ream_member_drag'
});